﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using SenacIntegra.Views;
using SenacIntegra.Services.Dados;
using SenacIntegra.Services;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace SenacIntegra
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();
            DadosServiceFactory.RegisterDependencies();

            MainPage = new NavigationPage(new MainPage());
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
