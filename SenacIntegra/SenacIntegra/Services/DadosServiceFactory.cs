﻿using SenacIntegra.Services.Dados;
using SenacIntegra.Services.Dados.Impl;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace SenacIntegra.Services
{
    static class DadosServiceFactory
    {
        public static void RegisterDependencies()
        {
            DependencyService.Register<ICursoDados, CursoDados>();
            DependencyService.Register<IProjetoDados, ProjetoDados>();
            DependencyService.Register<IUsuarioDados, UsuarioDados>();
        }

        public static ICursoDados Curso {
            get => DependencyService.Get<ICursoDados>();
        }
        public static IProjetoDados Projeto { get => DependencyService.Get<IProjetoDados>(); }
    }
}
