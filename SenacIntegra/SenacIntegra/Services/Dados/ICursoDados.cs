﻿using SenacIntegra.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SenacIntegra.Services.Dados
{
    public interface ICursoDados
    {
        List<Curso> GetCursos();
        Curso Get(int id);
    }
}
