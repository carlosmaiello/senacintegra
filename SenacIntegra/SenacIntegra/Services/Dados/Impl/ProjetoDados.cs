﻿using SenacIntegra.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace SenacIntegra.Services.Dados.Impl
{
    public class ProjetoDados : IProjetoDados
    {
        private Dictionary<int, Projeto> projetos = new Dictionary<int, Projeto>();
        private int UltimoId = 0;

        public ProjetoDados()
        {
            this.Salvar(new Projeto
            {
                Nome = "Projeto 1",
                Descricao = "Descricao do Projeto 1",
                Inicio = new DateTime(2019, 01, 01),
                Previsao = new DateTime(2019, 12, 31),
                Situacao = "andamento",
                Curso = DadosServiceFactory.Curso.Get(1)
            });

            this.Salvar(new Projeto
            {
                Nome = "Projeto 2",
                Descricao = "Descricao do Projeto 2",
                Inicio = new DateTime(2019, 02, 01),
                Previsao = new DateTime(2019, 11, 30),
                Situacao = "andamento",
                Curso = DadosServiceFactory.Curso.Get(1)
            });


            this.Salvar(new Projeto
            {
                Nome = "Projeto 3",
                Descricao = "Descricao do Projeto 3",
                Inicio = new DateTime(2019, 02, 01),
                Previsao = new DateTime(2019, 11, 30),
                Situacao = "andamento",
                Curso = DadosServiceFactory.Curso.Get(2)
            });

            this.Salvar(new Projeto
            {
                Nome = "Projeto 4",
                Descricao = "Descricao do Projeto 4",
                Inicio = new DateTime(2019, 02, 01),
                Previsao = new DateTime(2019, 11, 30),
                Situacao = "andamento",
                Curso = DadosServiceFactory.Curso.Get(3)
            });

            this.Salvar(new Projeto
            {
                Nome = "Projeto 5",
                Descricao = "Descricao do Projeto 5",
                Inicio = new DateTime(2019, 02, 01),
                Previsao = new DateTime(2019, 11, 30),
                Situacao = "andamento",
                Curso = DadosServiceFactory.Curso.Get(4)
            });

            this.Salvar(new Projeto
            {
                Nome = "Projeto 6",
                Descricao = "Descricao do Projeto 6",
                Inicio = new DateTime(2019, 02, 01),
                Previsao = new DateTime(2019, 11, 30),
                Situacao = "andamento",
                Curso = DadosServiceFactory.Curso.Get(4)
            });

            this.Salvar(new Projeto
            {
                Nome = "Projeto 7",
                Descricao = "Descricao do Projeto 6",
                Inicio = new DateTime(2019, 02, 01),
                Previsao = new DateTime(2019, 11, 30),
                Situacao = "andamento",
                Curso = DadosServiceFactory.Curso.Get(2)
            });

            this.Salvar(new Projeto
            {
                Nome = "Projeto 8",
                Descricao = "Descricao do Projeto 6",
                Inicio = new DateTime(2019, 02, 01),
                Previsao = new DateTime(2019, 11, 30),
                Situacao = "andamento",
                Curso = DadosServiceFactory.Curso.Get(4)
            });

        }

        public Projeto Get(int id)
        {
            Projeto projeto = null;
            projetos.TryGetValue(id, out projeto);
            return projeto;
        }

        public List<Projeto> GetProjetos()
        {
            return projetos.Values.ToList<Projeto>();
        }

        public List<Projeto> GetProjetosByCurso(Curso curso)
        {
            IEnumerable<Projeto> projs = from projeto in projetos.Values
                   where projeto.Curso.Id == curso.Id
                   select projeto;

            return projs.ToList();
        }

        public void Remover(Projeto projeto)
        {
            projetos.Remove(projeto.Id);
        }

        public void Salvar(Projeto projeto)
        {
            if (projeto.Id == 0)
            {
                projeto.Id = ++this.UltimoId;
                projetos.Add(projeto.Id, projeto);
            }
            else
            {
                if (!projetos.ContainsKey(projeto.Id))
                    throw new Exception("Chave não encontrada");

                projetos[projeto.Id] = projeto;
            }

        }
    }
}
