﻿using SenacIntegra.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SenacIntegra.Services.Dados.Impl
{
    public class CursoDados : ICursoDados
    {
        private Dictionary<int, Curso> cursos = new Dictionary<int, Curso>();

        public CursoDados()
        {
            cursos.Add(1, new Curso
            {
                Id = 1,
                Nome = "Técnico em Informática"
            });

            cursos.Add(2, new Curso
            {
                Id = 2,
                Nome = "Técnico em Enfermagem"
            });

            cursos.Add(3, new Curso
            {
                Id = 3,
                Nome = "Técnico em RH"
            });

            cursos.Add(4, new Curso
            {
                Id = 4,
                Nome = "Técnico em Segurança do Trabalho"
            });

        }

        public Curso Get(int id)
        {
            Curso curso = null;
            cursos.TryGetValue(id, out curso);
            return curso;
        }

        public List<Curso> GetCursos()
        {
            return cursos.Values.ToList();
        }
    }
}
