﻿using System;
using System.Collections.Generic;
using System.Text;
using SenacIntegra.Models;
using System.Linq;

namespace SenacIntegra.Services.Dados.Impl
{
    public class UsuarioDados : IUsuarioDados
    {
        private Dictionary<int, Usuario> usuarios = 
            new Dictionary<int, Usuario>();
        private int ultimoCodigo = 0;

        public void EsqueciSenha(string email)
        {
            throw new NotImplementedException();
        }

        public Usuario Get(int id)
        {
            return this.usuarios.ContainsKey(id) ?
                this.usuarios[id] : null;
        }

        public Usuario Logar(string email, string senha)
        {
            var res = from u in this.usuarios.Values
                      where u.Email == email &&
                            u.Senha == senha
                      select u;

            return res.First();
        }

        public void Salvar(Usuario usuario)
        {
            if (usuario.Id == 0)
            {
                this.ultimoCodigo += 1;
                usuario.Id = this.ultimoCodigo;
                this.usuarios.Add(usuario.Id, usuario);
            }
            else
            {
                this.usuarios[usuario.Id] = usuario;
            }

        }
    }
}
