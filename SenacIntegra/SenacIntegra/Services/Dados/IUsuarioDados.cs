﻿using System;
using System.Collections.Generic;
using System.Text;
using SenacIntegra.Models;

namespace SenacIntegra.Services.Dados
{
    public interface IUsuarioDados
    {
        void Salvar(Usuario usuario);
        Usuario Get(int id);
        Usuario Logar(string email, string senha);
        void EsqueciSenha(string email);
    }
}
