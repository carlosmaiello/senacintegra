﻿using SenacIntegra.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SenacIntegra.Services.Dados
{
    public interface IProjetoDados
    {
        /**
         * Retorna todos os projetos 
         */
        List<Projeto> GetProjetos();
        /**
         * Retorna todos os projetos de acordo com o curso selecionado
         */
        List<Projeto> GetProjetosByCurso(Curso curso);
        /**
         * Salva um projeto
         */
        void Salvar(Projeto projeto);

        /**
         * Remove um projeto
         */
        void Remover(Projeto projeto);

        /**
         * Retorna uma projeto com o ID
         */
        Projeto Get(int id);
        
    }
}
