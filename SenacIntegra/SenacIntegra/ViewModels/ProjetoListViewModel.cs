﻿using SenacIntegra.Models;
using SenacIntegra.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace SenacIntegra.ViewModels
{
    public class ProjetoListViewModel : BaseViewModel
    {
        private ObservableCollection<Projeto> _projetos;
        private ObservableCollection<Curso> _cursos;
        private Curso _cursoSelecionado;
        private ICommand _atualizarProjetosCommand;
        private ICommand _abrirProjetoCommand;

        public ObservableCollection<Projeto> Projetos { get => _projetos; set => SetProperty<ObservableCollection<Projeto>>(ref _projetos, value); }
        public ObservableCollection<Curso> Cursos { get => _cursos; set => SetProperty<ObservableCollection<Curso>>(ref _cursos, value); }
        public ICommand AbrirProjetoCommand { get => _abrirProjetoCommand; set => SetProperty(ref _abrirProjetoCommand, value); }
        public ICommand AtualizarProjetosCommand { get => _atualizarProjetosCommand; set => SetProperty(ref _atualizarProjetosCommand, value); }

        public Curso CursoSelecionado { get => _cursoSelecionado; set => SetProperty<Curso>(ref _cursoSelecionado, value); }

        public ProjetoListViewModel()
        {
            AbrirProjetoCommand = new Command<Projeto>(AbrirProjetoAction);
            AtualizarProjetosCommand = new Command(AtualizarProjetosAction);

            Projetos = new ObservableCollection<Projeto>();
            Cursos = new ObservableCollection<Curso>();

            DadosServiceFactory.Curso.GetCursos().ForEach(curso => Cursos.Add(curso));

            PropertyChanged += ProjetosViewModel_PropertyChanged;

            AtualizarProjetosAction();
        }

        private void ProjetosViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "CursoSelecionado")
            {
                AtualizarProjetosAction();
            }
        }

        private void AbrirProjetoAction(Projeto projeto)
        {
        }

        public void AtualizarProjetosAction()
        {
            new System.Threading.Thread(new System.Threading.ThreadStart(() =>
            {
                try
                {
                    if (IsBusy)
                        return;

                    IsBusy = true;

                    Thread.Sleep(4000);


                    Projetos.Clear();

                    if (this.CursoSelecionado == null)
                        DadosServiceFactory.Projeto.GetProjetos().ForEach(projeto => Projetos.Add(projeto));
                    else
                        DadosServiceFactory.Projeto.GetProjetosByCurso(this.CursoSelecionado).ForEach(projeto => Projetos.Add(projeto));

                }
                finally
                {
                    IsBusy = false;
                }
            })).Start();
        }
    }
}
