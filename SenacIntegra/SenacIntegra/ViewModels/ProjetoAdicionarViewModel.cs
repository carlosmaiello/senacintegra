﻿using SenacIntegra.Models;
using SenacIntegra.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace SenacIntegra.ViewModels
{
    public class ProjetoAdicionarViewModel : BaseViewModel
    {
        private ObservableCollection<Curso> _cursos;
        private string _nome;
        private string _descricao;
        private DateTime _inicio;
        private DateTime _previsao;
        private Curso _curso;

        public string Nome { get => _nome; set => SetProperty<string>(ref _nome, value); }
        public string Descricao { get => _descricao; set => SetProperty<string>(ref _descricao, value); }
        public DateTime Inicio { get => _inicio; set => SetProperty<DateTime>(ref _inicio, value); }
        public DateTime Previsao { get => _previsao; set => SetProperty<DateTime>(ref _previsao, value); }
        public Curso Curso { get => _curso; set => SetProperty<Curso>(ref _curso, value); }

        public ObservableCollection<Curso> Cursos { get => _cursos; set => SetProperty<ObservableCollection<Curso>>(ref _cursos, value); }
        public ICommand CadastrarCommand { get; set; }


        public ProjetoAdicionarViewModel()
        {
            CadastrarCommand = new Command(CadastrarAction);

            Cursos = new ObservableCollection<Curso>();

            DadosServiceFactory.Curso.GetCursos().ForEach(curso => Cursos.Add(curso));
            Inicio = DateTime.Now;
            Previsao = DateTime.Now;
        }

        private void CadastrarAction(object obj)
        {
            Projeto projeto;

            /*
            projeto = new Projeto
            {
                Nome = this.Nome,
                Descricao = this.Descricao,
                Inicio = this.Inicio,
                Previsao = this.Previsao,
                Termino = this.Previsao,
                Curso = this.Curso
            };
            */

            projeto = new Projeto();
            projeto.Nome = this.Nome;
            projeto.Descricao = this.Descricao;
            projeto.Inicio = this.Inicio;
            projeto.Previsao = this.Previsao;
            projeto.Termino = this.Previsao;
            projeto.Curso = this.Curso;

            DadosServiceFactory.Projeto.Salvar(projeto);

            this.Nome = "";
            this.Descricao = "";
            this.Inicio = DateTime.Now;
            this.Previsao = DateTime.Now;
            this.Curso = null;
        }
    }
}
