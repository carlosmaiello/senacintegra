﻿using SenacIntegra.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SenacIntegra.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ProjetoAdicionarPage : ContentPage
	{
		public ProjetoAdicionarPage ()
		{
			InitializeComponent ();
            BindingContext = new ProjetoAdicionarViewModel();
        }
	}
}