﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SenacIntegra.Models
{
    public class Projeto
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string Situacao { get; set; }
        public DateTime Inicio { get; set; }
        public DateTime Previsao { get; set; }
        public DateTime Termino { get; set; }

        /**
         * Atributos de Relacionamento
         */
         public List<Apresentacao> Apresentacoes { get; set; }
        public List<Usuario> Usuarios { get; set; }
        public Curso Curso { get; set; }
    }
}
