﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SenacIntegra.Models
{
    public class Curso
    {
        public int Id { get; set; }
        public string Nome { get; set; }
    }
}
