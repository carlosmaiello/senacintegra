﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SenacIntegra.Models
{
    public class Apresentacao
    {
        public int Id { get; set; }
        public DateTime Data { get; set; }
        public string Local { get; set; }
        public bool Publico { get; set; }
        public bool Final { get; set; }
    }
}
